// oscpkt library trigger secure-C-runtime warnings in Visual C++.
// Disable warnings locally
#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable: 4996)
#pragma warning(disable: 4267)
#pragma warning(disable: 4244)
#endif

#define OSCPKT_OSTREAM_OUTPUT
#include "../../../keepintouch/keepintouch/dependencies/oscpkt/oscpkt.hh"
#include "../../../keepintouch/keepintouch/dependencies/oscpkt/udp.hh"

#ifdef _MSC_VER
#pragma warning(pop)
#endif

using std::cout;
using std::cerr;

using namespace oscpkt;

const int PORT_NUM = 3333;

void runServer()
{
	UdpSocket sock; 
	sock.bindTo(PORT_NUM);
	if (!sock.isOk())
	{
		cerr << "Error opening port " << PORT_NUM << ": " << sock.errorMessage() << "\n";
	}
	else
	{
		cout << "Server started, will listen to packets on port " << PORT_NUM << std::endl;
		PacketReader pr;
		PacketWriter pw;

		std::string msgType;

		while (sock.isOk())
		{
			if (sock.receiveNextPacket(30 /* timeout, in ms */))
			{
				pr.init(sock.packetData(), sock.packetSize());
				oscpkt::Message *msg;
				while (pr.isOk() && (msg = pr.popMessage()) != 0)
				{
					if (oscpkt::Message::ArgReader arguments = msg->partialMatch("/tuio/2Dcur"))
					{
						arguments = arguments.popStr(msgType);
						if (msgType == "alive")
						{
							std::cout << "\tRecieved an alive msg!" << std::endl;
						}
						else if (msgType == "fseq")
						{
							std::cout << "\tRecieved a fseq msg!" << std::endl;
							int frame;
							arguments = arguments.popInt32(frame);
							std::cout << "\t\tframe: " << frame << std::endl;

						}
						else if (msgType == "set")
						{
							std::cout << "\tRecieved a set msg!" << std::endl;
							int id;
							float x, y, vx, vy, mAcc;

							if (arguments.popInt32(id).popFloat(x).popFloat(y).popFloat(vx).popFloat(vy).popFloat(mAcc).isOkNoMoreArgs())
							{
								std::cout << "id: " << id << std::endl;
								std::cout << "x: " << x << std::endl;
								std::cout << "y: " << y << std::endl;

								std::cout << "vx: " << vx << std::endl;
								std::cout << "vy: " << vy << std::endl;
								std::cout << "mAcc: " << mAcc << std::endl;
							}
							else
							{
								std::cout << "Error: Set msg bad formed!" << std::endl;
							}
						}
					}
					else
					{
						cout << "Server: unhandled message: " << *msg << "\n";
					}
				}
			}
		}
	}
}

int main(int argc, char **argv)
{
    runServer();
}