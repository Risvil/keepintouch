#ifndef KEEPINTOUCH_INTERFACES_H
#define KEEPINTOUCH_INTERFACES_H

namespace kit
{

class IObject
{
public:
	virtual void Init() = 0;
	virtual void Shutdown() = 0;

	virtual ~IObject() {}
};

class ISettings
{
public:
	virtual void Read() = 0;
	virtual void Write() = 0;

	virtual ~ISettings() {}
};

} // namespace kit

#endif // KEEPINTOUCH_INTERFACES_H