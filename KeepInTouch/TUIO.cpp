#include "TUIO.h"
#include <iostream>

namespace kit
{

using namespace oscpkt;

TUIO::TUIO()
:	m_frameseq(0),
	m_port(0),
	m_bIsConnected(false)
{
}

TUIO::~TUIO()
{
}

void TUIO::Init()
{
	Init("localhost", 3333);
}

void TUIO::Init(const std::string& host, int port)
{
	m_socket.connectTo(host, port);
	if (!m_socket.isOk())
	{
		std::cerr << "TUIO initialization failed: Cannot connect to specified host";
	}
}

void TUIO::Shutdown()
{
}

void TUIO::Send(const BlobContainer& blobs)
{
	if (!m_socket.isOk())
	{
		return;
	}

	m_frameseq++;

	oscpkt::PacketWriter writer;
	writer.startBundle();

	Message source("/tuio/2Dcur");
	source.pushStr("source kit@localhost");
	writer.addMessage(source);

	// if no blobs
	// TODO: Time delay between alive messages
	if (blobs.empty())
	{
		Message alive("/tuio/2Dcur");
		alive.pushStr("alive");
		alive.pushInt32(-1);

		writer.addMessage(alive);

		Message fseq("/tuio/2Dcur");
		fseq.pushStr("fseq");
		fseq.pushInt32(m_frameseq);

		writer.addMessage(fseq);
	}
	else
	{
		// Create Alive message
		Message alive("/tuio/2Dcur");
		alive.pushStr("alive");

		BlobContainer::const_iterator it;
		for (it = blobs.begin(); it != blobs.end(); ++it)
		{
			if (it->id == 0)
			{
				continue;
			}
			alive.pushInt32(it->id);
		}
		writer.addMessage(alive);

		// Create Set messages
		for (it = blobs.begin(); it != blobs.end(); ++it)
		{
			if (it->id == 0)
			{
				continue;
			}

			// Create Set message
			Message set("/tuio/2Dcur");

			set.pushStr("set");
			set.pushInt32(it->id);
			set.pushFloat(it->x);
			set.pushFloat(it->y);
			set.pushFloat(it->vx);
			set.pushFloat(it->vy);
			set.pushFloat(it->motionAcceleration);

			writer.addMessage(set);
		}

		Message fseq("/tuio/2Dcur");
		fseq.pushStr("fseq");
		fseq.pushInt32(m_frameseq);

		writer.addMessage(fseq);
	}

	writer.endBundle();

	bool bOk = m_socket.sendPacket(writer.packetData(), writer.packetSize());
	if (!bOk)
	{
		std::cerr << "Could not send long TUIO message" << std::endl;
	}
}

} // namespace kit