#ifndef TUIO_H
#define TUIO_H

#include <string>
#include "kit_common.h"
#include "kit_interfaces.h"

// oscpkt library trigger secure-C-runtime warnings in Visual C++.
// Disable warnings locally
#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable: 4996)
#pragma warning(disable: 4267)
#pragma warning(disable: 4244)
#endif

// oscpkt library headers
#include "dependencies/oscpkt/oscpkt.hh"
#include "dependencies/oscpkt/udp.hh"

#ifdef _MSC_VER
#pragma warning(pop)
#endif

namespace kit
{

class TUIO : public IObject
{
public:
	TUIO();
	~TUIO();

	void Init();
	void Init(const std::string& host, int port);
	void Shutdown();
	void Send(const BlobContainer& blobs);

private:
	std::string m_localHost;
	int m_port;
	bool m_bIsConnected;

	oscpkt::UdpSocket m_socket;

	int m_frameseq;
};

} // namespace kit

#endif // TUIO_H
