#ifndef FRAMEGRABBER_H
#define FRAMEGRABBER_H

#include <opencv2/opencv.hpp>
#include "kit_interfaces.h"

namespace kit
{

/**
 * Class used to grab frames from a camera source or an input video file.
 */
class FrameGrabber : public IObject
{
public:
	/**
	 * Class destructor
	 */
	virtual ~FrameGrabber();

	/**
	 * Initializes this component
	 */
	virtual void Init();

	/**
	 * Initializes the FrameGrabber to be used with a camera
	 */
	void Init(int cameraNumber);

	/**
	 * Initializes the FrameGrabber to be used with a video
	 */
	void Init(const std::string& videoPath);

	/**
	 * Shuts down the frame grabber, releasing the used resources properly
	 */
	virtual void Shutdown();

	/**
	 * Sets the desired frame size for the retrieved frames
	 */
	void SetFrameSize(int width, int height);

	/**
	 * Returns the next frame in the input stream
	 */
	const cv::Mat& GetNextFrame();
	
private:
	cv::VideoCapture m_capture;
	cv::Mat m_captureFrame;
};

} // namespace kit

#endif // FRAMEGRABBER_H
