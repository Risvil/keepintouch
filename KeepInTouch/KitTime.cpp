#include <iostream>
#include <cstdlib>
#include <sys/timeb.h>
#include "KitTime.h"

using namespace std;

namespace kit
{

int KitTime::GetMilliCount()
{
    timeb tb;
    ftime(&tb);
    int nCount = tb.millitm + (tb.time & 0xfffff) * 1000;
    return nCount;
}

int KitTime::GetMilliSpan(int nTimeStart)
{
    int nSpan = GetMilliCount() - nTimeStart;
    if(nSpan < 0)
	{
        nSpan += 0x100000 * 1000;
	}
    return nSpan;
}

} // namespace kit
