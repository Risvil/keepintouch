#ifndef KIT_UTILS_H
#define KIT_UTILS_H

namespace kit
{

#define SAFE_RELEASE(x) if (x != NULL) { delete x; x = NULL; }

} // namespace kit

#endif // KIT_UTILS_H