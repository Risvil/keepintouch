#ifndef KEEPINTOUCHTRACKER_H
#define KEEPINTOUCHTRACKER_H

#include "kit_interfaces.h"
#include "FrameGrabber.h"
#include "BlobFilterer.h"
#include "BlobDetector.h"
#include "BlobTracker.h"
#include "Calibration.h"

namespace kit
{

class TUIO;
class Tracker : public IObject
{
public:
	/**
	 * Class basic constructor
	 */
	Tracker();

	/**
	 * Class destructor
	 */
	virtual ~Tracker();

	/**
	 * Initializes this component
	 */
	virtual void Init();

	/**
	 * Shuts down this component and releases resources
	 */
	virtual void Shutdown();

	/**
	 * Gets the calibration method being used on blob detection
	 */
	inline ICalibrationMethod* GetCalibrator() const
	{
		return m_detector.GetCalibrator();
	}

	/**
	 * Performs the full detection process, from image adquiring to blob detection
	 */
	void PerformDetection();

	/**
	 * Gets the current threshold value
	 */
	int GetThreshold() const;

	/**
	 * Sets threshold value
	 */
	void SetThreshold(int value);

	/**
	 * Gets the current smooth value
	 */
	int GetSmooth() const;

	/**
	 * Sets smooth value
	 */
	void SetSmooth(int value);

	/**
	 * Gets the current blur value
	 */
	int GetBlur() const;

	/**
	 * Sets blur value
	 */
	void SetBlur(int value);

	/**
	 * Gets the current noise value
	 */
	int GetNoise() const;

	/**
	 * Sets noise value
	 */
	void SetNoise(int value);

	/**
	 * Gets the current amplify level
	 */
	int GetAmplifyLevel() const;

	/**
	 * Sets amplify level
	 */
	void SetAmplifyLevel(int value);

	/**
	 * Gets the minimum allowed size for a blob to be considered of interest
	 */
	int GetBlobMinAllowedSize() const;

	/**
	 * Sets the minimum allowed size for a blob to be considered of interest
	 */
	void SetBlobMinAllowedSize(int value);

	/**
	 * Gets the maximum allowed size for a blob to be considered of interest
	 */
	int GetBlobMaxAllowedSize() const;

	/**
	 * Sets the maximum allowed size for a blob to be considered of interest
	 */
	void SetBlobMaxAllowedSize(int value);

	/**
	 * Gets the maximum threshold value acceptable
	 */
	int GetMaxThresholdValue() const;

	/**
	 * Gets the maximum smooth value acceptable
	 */
	int GetMaxSmoothLevel() const;

	/**
	 * Gets the maximum blur value acceptable
	 */
	int GetMaxBlurLevel() const;

	/**
	 * Gets the maximum noise value acceptable
	 */
	int GetMaxNoiseLevel() const;

	/**
	 * Gets the maximum amplify value acceptable
	 */
	int GetMaxAmplifyLevel() const;

	/**
	 * Gets the maximum blob size acceptable
	 */
	int GetMaxBlobSize() const;

	/**
	 * Stores the partial frames. They can be used, for example, for
	 * showing the intermediate steps in detection inside a GUI.
	 */
	void SetStorePartialFrames(bool bStore);

	/**
	 * Sets the current frame as the background frame that will be 
	 * used in background substraction
	 */
	void SetCurrentInputFrameAsBackground();

	/**
	 * Gets the input frame
	 */
	inline const cv::Mat& GetInputFrame() const
	{
		return m_filterer.GetInputFrame();
	}

	/**
	 * Gets the background frame
	 */
	inline const cv::Mat& GetBackgroundFrame() const
	{
		return m_filterer.GetBackgroundFrame();
	}

	/**
	 * Gets the result frame
	 */
	inline const cv::Mat& GetResultFrame() const
	{
		return m_filterer.GetResultFrame();
	}

	/**
	 * Gets the smooth frame
	 */
	inline const cv::Mat& GetSmoothedFrame() const
	{
		return m_filterer.GetSmoothedFrame();
	}

	/**
	 * Gets the highpass frame
	 */
	inline const cv::Mat& GetHighpassedFrame() const
	{
		return m_filterer.GetHighpassedFrame();
	}

	/**
	 * Gets the amplify frame
	 */
	inline const cv::Mat& GetAmplifiedFrame() const
	{
		return m_filterer.GetAmplifiedFrame();
	}

	/**
	 * Gets the input blobs that will be tracked
	 */
	inline const BlobDetector::Contours& GetInputBlobs() const
	{
		return m_detector.GetBlobs();
	}

	/**
	 * Gets the blobs already tracked and labeled
	 */
	inline const BlobContainer& GetOutputBlobs() const
	{
		return m_pTracker->GetOutputBlobs();
	}

private:
	/**
	 * Grabs a frame from the input source
	 */	
	const cv::Mat& GrabFrame();

	/**
	 * Performs a filtering procedure from input image and
	 * returns an image containing the resulting blobs
	 */
	const cv::Mat& FilterBlobs(const cv::Mat& image);

	/**
	 * Detects all the blobs found in source image, and
	 * returns the result
	 */
	const BlobContainer& DetectBlobs(const cv::Mat& image);

	/**
	 * Keeps a track of the input blobs and returns back
	 * labeled output.
	 */
	const BlobContainer&  TrackBlobs(BlobContainer& blobs);

	/**
	 * Sends blobs using TUIO protocol
	 */
	void SendBlobs(const BlobContainer& blobs);

private:
	// Main components
	FrameGrabber m_grabber;
	BlobFilterer m_filterer;
	BlobDetector m_detector;
	ABlobTracker* m_pTracker;
	TUIO* m_pTuio;
};

} // namespace kit

#endif // KEEPINTOUCHTRACKER_H