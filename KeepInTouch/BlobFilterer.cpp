#include "BlobFilterer.h"

namespace kit
{

/**
 * Constants
 */
const int BlobFilterer::MAX_THRESHOLD_VALUE = 255;
const int BlobFilterer::MAX_SMOOTH_LEVEL = 10;
const int BlobFilterer::MAX_BLUR_LEVEL = 100;
const int BlobFilterer::MAX_NOISE_LEVEL = 10;
const int BlobFilterer::MAX_AMPLIFY_LEVEL = 20;

/**
 * Class basic constructor
 */
BlobFilterer::BlobFilterer()
	: m_bStorePartialFrames(true)
{
	m_thresholdValue = static_cast<int>( MAX_THRESHOLD_VALUE * 0.6f );

	m_smooth = MAX_SMOOTH_LEVEL / 2;
	m_blur = MAX_BLUR_LEVEL / 3;
	m_noise = MAX_NOISE_LEVEL / 4;
	m_amplifyLevel = MAX_AMPLIFY_LEVEL / 2;
}

/**
 * Class destructor
 */
BlobFilterer::~BlobFilterer()
{
}

/**
 * Initializes this component
 */
void BlobFilterer::Init()
{
}

/**
 * Shuts down this component and releases resources
 */
void BlobFilterer::Shutdown()
{
}

/**
 * Filters the image to improve further detection of blobs
 */
bool BlobFilterer::FilterBlobs()
{
	if (m_inputFrame.empty())
	{
		return false;
	}

	cv::Mat grayFrame;

	// Convert to gray
	cvtColor(m_inputFrame, grayFrame, CV_BGR2GRAY);

	// Store background image, if needed
	if (NeedsBackgroundFrame())
	{
		m_backgroundFrame = grayFrame;
	}

	// Remove background
	RemoveBackground(grayFrame, m_resultFrame);

	// Smooth the image
	Smooth(m_resultFrame, m_resultFrame, m_smooth);
	SaveSmoothedImage();

	// Highpass
	Highpass(m_resultFrame, m_resultFrame, m_blur, m_noise);
	SaveHighpassedImage();

	// Amplify
	Amplify(m_resultFrame, m_resultFrame, m_amplifyLevel);
	SaveAmplifiedImaged();

	// Segment the image
	ApplyThreshold(m_resultFrame, m_resultFrame, m_thresholdValue);

	// Remove noise
	RemoveNoise(m_resultFrame, m_resultFrame);
	
	return true;
}

/**
 * Performs a highpass filter to the input image, and returns the result
 */
void BlobFilterer::Highpass(const cv::Mat& source, cv::Mat& dest, int blur, int noise)
{
	cv::Mat temp;

	// Blur original image
	if (blur > 0)
	{
		int blurKernelSize = (blur*2) + 1;
		cv::blur(source, temp, cv::Size(blurKernelSize, blurKernelSize));
	}
	else
	{
		temp = source;
	}

	// Highpass = original - blurred
	dest = source - temp;

	// Remove noise from Highpass
	if (noise > 0)
	{
		int noiseKernelSize = (noise*2) + 1;
		cv::blur(dest, dest, cv::Size(noiseKernelSize, noiseKernelSize));
	}
}

} // namespace kit