#include "BlobDetector.h"
#include "kit_utils.h"
#include "Calibration.h"

#include <string>
#define _USE_MATH_DEFINES
#include <math.h>

namespace kit
{

const int BlobDetector::MIN_BLOB_SIZE = 0;
const int BlobDetector::MAX_BLOB_SIZE = 1000;

/**
 * Basic constructor
 */
BlobDetector::BlobDetector()
{
	m_blobMinAllowedSize = MIN_BLOB_SIZE;
	m_blobMaxAllowedSize = MAX_BLOB_SIZE;

	m_pCalibrator = new LinearInterpolationCalibrationMethod();
}

/**
 * Class destructor
 */
BlobDetector::~BlobDetector()
{
}

/**
 * Initializes this component
 */
void BlobDetector::Init()
{
}

/**
 * Shuts down this component and releases resources
 */
void BlobDetector::Shutdown()
{
}

/**
 * Detects blobs in an image
 */
void BlobDetector::DetectBlobs()
{
	Contours allContours;
	cv::findContours(m_inputFrame, allContours, CV_RETR_LIST, CV_CHAIN_APPROX_NONE);

	m_blobs.clear();
	for (int i = 0; i < allContours.size(); ++i)
	{
		cv::Mat blob(allContours[i]);

		float blobArea = static_cast<float>( cv::contourArea(blob) );

		if (blobArea >= m_blobMinAllowedSize && blobArea <= m_blobMaxAllowedSize)
		{
			// TODO: Label blob individually here
			m_blobs.push_back(blob);
		}
	}

	LabelBlobs();
}

/**
 * Sets up all the additional information needed for a full labeled blob, from the unlabeled ones.
 */
void BlobDetector::LabelBlobs()
{
	m_labeledBlobs.clear();
	for (int i = 0; i < m_blobs.size(); ++i)
	{
		Contour currentContour = m_blobs[i];

		// Calculate center, width and height
		float radius;
		cv::Point2f center;
		cv::minEnclosingCircle(currentContour, center, radius);

		Blob currentBlob;

		// Normalize position and apply calibration transform
		currentBlob.x = center.x / m_inputFrame.cols;
		currentBlob.y = center.y / m_inputFrame.rows;
		ApplyCalibration(currentBlob);

		m_labeledBlobs.push_back(currentBlob);
	}
}

/**
 * Applies calibration method to a blob position
 */
void BlobDetector::ApplyCalibration(Blob& blob) const
{
	if (m_pCalibrator != NULL && !m_pCalibrator->IsCalibrationMode())
	{
		m_pCalibrator->ApplyCalibration(blob);
	}
}

} // namespace kit