#include "Tracker.h"
#include "kit_utils.h"
#include "TUIO.h"

namespace kit
{

/**
 * Class basic constructor
 */
Tracker::Tracker()
{
	m_pTracker = new BlobTrackerKNN();
	m_pTuio = new TUIO();
}

/**
 * Class destructor
 */
Tracker::~Tracker()
{
	SAFE_RELEASE(m_pTracker);
	SAFE_RELEASE(m_pTuio);
}

/**
 * Initializes this component
 */
void Tracker::Init()
{
	m_grabber.Init();
	m_filterer.Init();
	m_detector.Init();
	m_pTracker->Init();
	m_pTuio->Init();
}

/**
 * Shuts down this component and releases resources
 */
void Tracker::Shutdown()
{
	m_grabber.Shutdown();
	m_filterer.Shutdown();
	m_detector.Shutdown();
	m_pTracker->Shutdown();
	m_pTuio->Shutdown();
}

/**
 * Performs the full detection process, from image adquiring to blob detection
 */
void Tracker::PerformDetection()
{
	const cv::Mat cameraFrame = GrabFrame();
	const cv::Mat filteredFrame = FilterBlobs(cameraFrame);
	BlobContainer labeledBlobs = DetectBlobs(filteredFrame);
	BlobContainer trackedBlobs = TrackBlobs(labeledBlobs);

	// Update calibrator if calibration mode is active
	ICalibrationMethod* calibrator = GetCalibrator();
	if (calibrator != NULL && calibrator->IsCalibrationMode())
	{
		calibrator->UpdateCalibration(trackedBlobs);

		// Do not send calibration blobs
		trackedBlobs = BlobContainer();
	}

	SendBlobs(trackedBlobs);
}

/**
 * Grabs a frame from the input source
 */	
const cv::Mat& Tracker::GrabFrame()
{
	return m_grabber.GetNextFrame();
}

/**
 * Performs a filtering procedure from input image and
 * returns an image containing the resulting blobs
 */
const cv::Mat& Tracker::FilterBlobs(const cv::Mat& image)
{
	m_filterer.SetInputFrame(image);
	m_filterer.FilterBlobs();
	return m_filterer.GetResultFrame();
}

/**
 * Detects all the blobs found in source image, and
 * returns the result
 */
const BlobContainer& Tracker::DetectBlobs(const cv::Mat& image)
{	
	if (!image.empty())
	{
		cv::Mat preProcessedFrame = image.clone();
		m_detector.SetInputFrame(preProcessedFrame);
		m_detector.DetectBlobs();
	}

	return m_detector.GetLabeledBlobs();
}

/**
 * Keeps a track of the input blobs and returns back
 * labeled output.
 */
const BlobContainer& Tracker::TrackBlobs(BlobContainer& blobs)
{
	m_pTracker->SetInputBlobs(blobs);
	m_pTracker->TrackBlobs();

	return m_pTracker->GetOutputBlobs();
}

/**
 * Sends blobs using TUIO protocol
 */
void Tracker::SendBlobs(const BlobContainer& blobs)
{
	m_pTuio->Send(blobs);
}

/**
 * Gets the current threshold value
 */
int Tracker::GetThreshold() const
{
	return m_filterer.GetThreshold();
}

/**
 * Sets threshold value
 */
void Tracker::SetThreshold(int value)
{
	m_filterer.SetThreshold(value);
}

/**
 * Gets the current smooth value
 */
int Tracker::GetSmooth() const
{
	return m_filterer.GetSmooth();
}

/**
 * Sets smooth value
 */
void Tracker::SetSmooth(int value)
{
	m_filterer.SetSmooth(value);
}

/**
 * Gets the current blur value
 */
int Tracker::GetBlur() const
{
	return m_filterer.GetBlur();
}

/**
 * Sets blur value
 */
void Tracker::SetBlur(int value)
{
	m_filterer.SetBlur(value);
}

/**
 * Gets the current noise value
 */
int Tracker::GetNoise() const
{
	return m_filterer.GetNoise();
}

/**
 * Sets noise value
 */
void Tracker::SetNoise(int value)
{
	m_filterer.SetNoise(value);
}

/**
 * Gets the current amplify level
 */
int Tracker::GetAmplifyLevel() const
{
	return m_filterer.GetAmplifyLevel();
}

/**
 * Sets amplify level
 */
void Tracker::SetAmplifyLevel(int value)
{
	m_filterer.SetAmplifyLevel(value);
}

/**
 * Gets the minimum allowed size for a blob to be considered of interest
 */
int Tracker::GetBlobMinAllowedSize() const
{
	return m_detector.GetBlobMinAllowedSize();
}

/**
 * Sets the minimum allowed size for a blob to be considered of interest
 */
void Tracker::SetBlobMinAllowedSize(int value)
{
	m_detector.SetBlobMinAllowedSize(value);
}

/**
 * Gets the maximum allowed size for a blob to be considered of interest
 */
int Tracker::GetBlobMaxAllowedSize() const
{
	return m_detector.GetBlobMaxAllowedSize();
}

/**
 * Sets the maximum allowed size for a blob to be considered of interest
 */
void Tracker::SetBlobMaxAllowedSize(int value)
{
	m_detector.SetBlobMaxAllowedSize(value);
}

/**
 * Gets the maximum threshold value acceptable 
 */
int Tracker::GetMaxThresholdValue() const
{
	return m_filterer.MAX_THRESHOLD_VALUE;
}

/**
 * Gets the maximum smooth value acceptable
 */
int Tracker::GetMaxSmoothLevel() const
{
	return m_filterer.MAX_SMOOTH_LEVEL;
}

/**
 * Gets the maximum blur value acceptable
 */
int Tracker::GetMaxBlurLevel() const
{
	return m_filterer.MAX_BLUR_LEVEL;
}

/**
 * Gets the maximum noise value acceptable
 */
int Tracker::GetMaxNoiseLevel() const
{
	return m_filterer.MAX_NOISE_LEVEL;
}

/**
 * Gets the maximum amplify value acceptable
 */
int Tracker::GetMaxAmplifyLevel() const
{
	return m_filterer.MAX_AMPLIFY_LEVEL;
}

/**
 * Gets the maximum blob size acceptable
 */
int Tracker::GetMaxBlobSize() const
{
	return m_detector.MAX_BLOB_SIZE;
}

/**
 * Stores the partial frames. They can be used, for example, for
 * showing the intermediate steps in detection inside a GUI.
 */
void Tracker::SetStorePartialFrames(bool bStore)
{
	m_filterer.SetStorePartialFrames(bStore);
}

/**
 * Sets the current frame as the background frame that will be 
 * used in background substraction
 */
void Tracker::SetCurrentInputFrameAsBackground()
{
	m_filterer.SetCurrentInputFrameAsBackground();
}

} // namespace kit