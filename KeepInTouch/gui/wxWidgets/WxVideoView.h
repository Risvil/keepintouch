#ifndef WXVIDEOVIEW_H
#define WXVIDEOVIEW_H

#include "WxKitGUI.h"

#if USE_WX_WINDOW_GUI

namespace kit
{

class WxVideoView : public wxWindow
{
public:
	WxVideoView(wxWindow *parent, wxWindowID id, const wxPoint &pos=wxDefaultPosition, const wxSize &size=wxDefaultSize, long style=0, const wxString &name=wxPanelNameStr);

	void Draw(wxDC& dc);
	void DrawVideoFrame(cv::Mat frame);

	void SetVideoFrame(const cv::Mat& frame);

private:
	void OnPaint(wxPaintEvent& e);
	void OnSize(wxSizeEvent& e);
	wxDECLARE_EVENT_TABLE();

private:
	int m_width;
	int m_height;

	bool m_bUpdateFrames;

	cv::Mat m_videoFrame;
	wxMutex m_frameMutex;
};

} // namespace kit

#endif // USE_WX_WINDOW_GUI

#endif // WXVIDEOVIEW_H