#include "WxVideoView.h"

#if USE_WX_WINDOW_GUI

namespace kit
{

wxBEGIN_EVENT_TABLE(WxVideoView, wxWindow)
    EVT_PAINT(WxVideoView::OnPaint)
    EVT_SIZE(WxVideoView::OnSize)
wxEND_EVENT_TABLE()

WxVideoView::WxVideoView(wxWindow *parent, wxWindowID id, const wxPoint &pos, const wxSize &size, long style, const wxString &name)
: wxWindow(parent, id, pos, size, style, wxPanelNameStr),
  m_width(0),
  m_height(0),
  m_bUpdateFrames(false)
{
}

void WxVideoView::Draw(wxDC& dc)
{
	m_frameMutex.Lock();
	
	if (!m_videoFrame.empty())
	{
		// wxBitmap only works with RGB images. Converting grayscale images
		if (m_videoFrame.channels() == 1)
		{
			cv::Mat channels[] = { m_videoFrame, m_videoFrame, m_videoFrame };
			cv::merge(channels, 3, m_videoFrame);
		}

		// Copy raw data to wxBitmap
		wxImage pWxImg = wxImage(m_videoFrame.size().width, m_videoFrame.size().height, m_videoFrame.data, TRUE);
		if (pWxImg.IsOk())
		{
			// Rescale image to fit widget size
			wxBitmap bitmap;
			if (m_width > 0 && m_height > 0)
			{
				bitmap = wxBitmap(pWxImg.Scale(m_width, m_height));
			}
			else
			{
				bitmap = wxBitmap(pWxImg);
			}

			// Draw wxBitmap
			if (dc.IsOk())
			{
				int x, y, w, h;
				dc.GetClippingBox(&x, &y, &w, &h);

				dc.DrawBitmap(bitmap, x, y);
			}
		}
	}

	m_frameMutex.Unlock();
}

void WxVideoView::SetVideoFrame(const cv::Mat& frame)
{
	if (!frame.empty())
	{
		m_frameMutex.Lock();
		m_videoFrame = frame;
		m_frameMutex.Unlock();

		Refresh(false);
		Update();
	}
}

void WxVideoView::OnPaint(wxPaintEvent& e)
{
	wxPaintDC dc(this);
	Draw(dc);
}

void WxVideoView::OnSize(wxSizeEvent& e)
{
	m_width = e.GetSize().GetWidth();
	m_height = e.GetSize().GetHeight();
}

} // namespace kit

#endif // USE_WX_WINDOW_GUI