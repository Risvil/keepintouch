#include "WxKitCalibrationFrame.h"

#if USE_WX_WINDOW_GUI

#include "WxVideoView.h"
#include "../../Tracker.h"
#include "../../Calibration.h"

namespace kit
{

wxBEGIN_EVENT_TABLE(WxKitCalibrationFrame, wxFrame)
	EVT_KEY_UP(WxKitCalibrationFrame::OnKeyReleased)
	EVT_CLOSE(WxKitCalibrationFrame::OnClose)
wxEND_EVENT_TABLE()

WxKitCalibrationFrame::WxKitCalibrationFrame(wxWindow* parent, wxWindowID id, const wxString& title, KeepInTouch* kit)
: wxFrame(parent, id, title, wxDefaultPosition, wxDefaultSize),
  m_pKit(kit)
{
	m_pVideo = new WxVideoView(this, wxID_ANY, wxDefaultPosition);
}

WxKitCalibrationFrame::~WxKitCalibrationFrame(void)
{
}

void WxKitCalibrationFrame::RegisterCalibrationEventListener(IWxCalibrationEventListener* listener)
{
	m_listeners.push_back(listener);
}

void WxKitCalibrationFrame::Update()
{
	if (m_pKit != NULL && m_pKit->IsCalibrationMode())
	{
		// Draw output
		Tracker* tracker = m_pKit->GetTracker();
		if (tracker != NULL && tracker->GetInputFrame().empty())
		{
			return;
		}

		cv::Mat combinedOutput = cv::Mat::zeros(m_pKit->GetTracker()->GetInputFrame().size(), CV_8UC3);

		DrawDetectionOutput(combinedOutput);
		DrawTrackingOutput(combinedOutput);

		// Draw current calibration point
		ICalibrationMethod* calibrator = m_pKit->GetCalibrator();
		if (calibrator != NULL && !calibrator->IsCalibrated())
		{
			// Get image width and height
			int screenWidth = combinedOutput.size().width;
			int screenHeight = combinedOutput.size().height;

			FloatPoint p = calibrator->GetCurrentCalibrationPoint();

			// Scale point
			cv::Point2i cvp = cv::Point(p.x * screenWidth, p.y * screenHeight);

			// Set point intensity based on calibration completion percentage
			float completionPercentage = calibrator->GetCurrentPointCalibrationPercentage();
			cv::Scalar color(255, 255, 255);
			color[1] *= completionPercentage;
			color[2] *= completionPercentage;

			// Draw p into window
			const int circleRadius = static_cast<int>(screenWidth * 0.05f);
			cv::circle(combinedOutput, cvp, circleRadius, color, -1);

			SetVideoFrame(combinedOutput);
		}
		else
		{
			// Calibration completed
			StopCalibration(true);
		}
	}
}

void WxKitCalibrationFrame::SetVideoFrame(const cv::Mat& frame)
{
	if (m_pVideo != NULL)
	{
		m_pVideo->SetVideoFrame(frame);
	}
}

void WxKitCalibrationFrame::OnKeyReleased(wxKeyEvent& event)
{
	switch (event.GetKeyCode())
	{
	case WXK_ESCAPE:
		StopCalibration(false);
		break;
	}
}

void WxKitCalibrationFrame::OnClose(wxCloseEvent&)
{
	StopCalibration(false);
}

void WxKitCalibrationFrame::StartCalibration()
{
	Show();
	ShowFullScreen(true);
	SetFocus();

	if (m_pKit != NULL)
	{
		m_pKit->StartCalibration();
	}
}

void WxKitCalibrationFrame::StopCalibration(bool bSuccessful)
{
	// Stop calibration and close window only once
	if (m_pKit != NULL && m_pKit->IsCalibrationMode())
	{
		m_pKit->StopCalibration();
	}

	// Notify observers
	for (int i = 0; i < m_listeners.size(); ++i)
	{
		IWxCalibrationEventListener* listener = m_listeners[i];
		if (listener != NULL)
		{
			listener->OnCalibrationComplete(bSuccessful);
		}
	}

	Hide();
}

// Drawing
void WxKitCalibrationFrame::DrawDetectionOutput(cv::Mat& drawing)
{
	cv::Scalar contourColor = cv::Scalar(0, 0, 255);
	cv::Scalar rectangleColor = cv::Scalar(0, 255, 0);
	cv::Scalar centerColor = cv::Scalar(255, 0, 0);

	const BlobDetector::Contours blobs = m_pKit->GetTracker()->GetInputBlobs();
	for (int i = 0; i < blobs.size(); ++i)
	{
		cv::drawContours(drawing, blobs, i, contourColor, 2);

		cv::Mat blob(blobs[i]);

		// Draw bounding rectangle
		cv::Rect rect(cv::boundingRect(blob));
		cv::rectangle(drawing, rect.tl(), rect.br(), rectangleColor, 2);

		// Draw center
		cv::Point2f center;
		float radius;
		cv::minEnclosingCircle(blob, center, radius);
		cv::circle(drawing, center, 1, centerColor, 2);
	}
}

void WxKitCalibrationFrame::DrawTrackingOutput(cv::Mat& drawing)
{
	cv::Scalar textColor = cv::Scalar(255, 255, 255);

	BlobContainer trackedBlobs = m_pKit->GetTracker()->GetOutputBlobs();
	for (int i = 0; i < trackedBlobs.size(); ++i)
	{
		Blob currentBlob = trackedBlobs[i];
		std::ostringstream buffer;
		buffer << currentBlob.id;
		std::string areaText = buffer.str();

		// Denormalize position
		cv::Point center(static_cast<int>(currentBlob.x * drawing.rows), static_cast<int>(currentBlob.y * drawing.cols));
		cv::putText(drawing, areaText, center, CV_FONT_HERSHEY_SIMPLEX, 0.5f, textColor);
	}
}

} // namespace kit

#endif // USE_WX_WINDOW_GUI
