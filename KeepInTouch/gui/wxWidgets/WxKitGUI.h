#ifndef WXKITGUI_H
#define WXKITGUI_H

#include "../../Build.h"

#if USE_WX_WINDOW_GUI

#include "../../kit_interfaces.h"
#include "../../KeepInTouch.h"
//#include "opencv2/opencv.hpp"

#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/core/core.hpp"

#pragma message ("Sdfasfd")

// For compilers that support precompilation, includes "wx/wx.h".
#include <wx/wxprec.h>
#ifndef WX_PRECOMP
    #include <wx/wx.h>
#endif

#include "WxKitApp.h"

#endif // USE_WX_WINDOW_GUI

#endif // WXKITGUI_H
