#ifndef WXKITAPP_H
#define WXKITAPP_H

#include "WxKitGUI.h"

#if USE_WX_WINDOW_GUI

#pragma message("[1] Compiling KIT_UI with wxWidgets")

namespace kit
{

class WxKitApp : public wxApp
{
public:
    virtual bool OnInit();
	virtual int OnExit();
};

} // namespace kit

#endif // USE_WX_WINDOW_GUI

#endif // WXKITAPP_H
