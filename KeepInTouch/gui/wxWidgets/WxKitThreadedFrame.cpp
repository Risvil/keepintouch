#include "WxKitThreadedFrame.h"

#if USE_WX_WINDOW_GUI

#include "WxVideoView.h"
#include "WxKitCalibrationFrame.h"

#include "../../kit_utils.h"
#include "../../KeepInTouch.h"
#include "../../Tracker.h"

namespace kit
{
wxDECLARE_EVENT(wxEVT_COMMAND_KIT_UPDATE, wxThreadEvent);
wxDEFINE_EVENT(wxEVT_COMMAND_KIT_UPDATE, wxThreadEvent);

wxBEGIN_EVENT_TABLE(WxKitThreadedFrame, wxFrame)
	EVT_THREAD(wxEVT_COMMAND_KIT_UPDATE, WxKitThreadedFrame::OnKitUpdate)

	EVT_CLOSE(WxKitThreadedFrame::OnClose)

	EVT_MENU(wxID_OPEN, WxKitThreadedFrame::OnSettingsLoad)
	EVT_MENU(wxID_SAVE, WxKitThreadedFrame::OnSettingsSave)
	EVT_MENU(MID_Background, WxKitThreadedFrame::OnBackgroundButtonPress)
	EVT_MENU(MID_Calibrate, WxKitThreadedFrame::OnCalibrationStart)
	EVT_MENU(wxID_EXIT,  WxKitThreadedFrame::OnExit)
    EVT_MENU(wxID_ABOUT, WxKitThreadedFrame::OnAbout)

	EVT_SLIDER(SID_Threshold, WxKitThreadedFrame::OnThresholdUpdate)
	EVT_SLIDER(SID_Smooth, WxKitThreadedFrame::OnSmoothUpdate)
	EVT_SLIDER(SID_Blur, WxKitThreadedFrame::OnBlurUpdate)
	EVT_SLIDER(SID_Noise, WxKitThreadedFrame::OnNoiseUpdate)
	EVT_SLIDER(SID_AmplifyLevel, WxKitThreadedFrame::OnAmplifyLevelUpdate)
	EVT_SLIDER(SID_MinBlobSize, WxKitThreadedFrame::OnMinBlobSizeUpdate)
	EVT_SLIDER(SID_MaxBlobSize, WxKitThreadedFrame::OnMaxBlobSizeUpdate)

	EVT_BUTTON(BID_Background, WxKitThreadedFrame::OnBackgroundButtonPress)

	EVT_TIMER(TiID_StatusUpdate, WxKitThreadedFrame::OnStatusUpdateTimer)
	EVT_TIMER(TiID_NoCameraInput, WxKitThreadedFrame::OnNoCameraInputTimer)
wxEND_EVENT_TABLE()

WxKitThreadedFrame::WxKitThreadedFrame(const wxString& title, const wxPoint& pos, const wxSize& size)
: wxFrame(NULL, wxID_ANY, title, pos, size)
{
	m_pVideo = NULL;
	m_pBackgroundFrame = NULL;
	m_pResultFrame = NULL;
	m_pSmoothFrame = NULL;
	m_pHighpassFrame = NULL;
	m_pAmplifyFrame = NULL;
	m_pCalibrationFrame = NULL;

	m_bWorkerAlive = true;

	m_pKit = new KeepInTouch();

	Bind(wxEVT_THREAD, (wxObjectEventFunction)&WxKitThreadedFrame::OnKitInitialized, this, TID_Initialize);
	Bind(wxEVT_THREAD, (wxObjectEventFunction)&WxKitThreadedFrame::OnKitUpdate, this, TID_Update);	

	m_statusUpdateTimer = new wxTimer(this, TiID_StatusUpdate);
	m_noCameraInputTimer = new wxTimer(this, TiID_NoCameraInput);

	// Sample menu and status bar
    wxMenu *menuFile = new wxMenu;
    menuFile->Append(wxID_OPEN, "&Load settings\tCtrl-L",
                     "Load the settings file");
    menuFile->Append(wxID_SAVE, "&Save settings\tCtrl-S",
                     "Save current settings to file");    
	menuFile->AppendSeparator();
    menuFile->Append(wxID_EXIT, "&Quit\tCtrl-Q",
					 "Exits the application");

	wxMenu* menuEdit = new wxMenu;
	menuEdit->Append(MID_Background, "Capture &background\tCtrl-B",
					 "Capture current frame as the background");
	menuEdit->Append(MID_Calibrate, "Calibrate &camera\tCtrl-C",
					 "Start camera calibration mode");

    wxMenu *menuHelp = new wxMenu;
    menuHelp->Append(wxID_ABOUT);

    wxMenuBar *menuBar = new wxMenuBar;
    menuBar->Append( menuFile, "&File" );
	menuBar->Append( menuEdit, "&Edit" );
    menuBar->Append( menuHelp, "&Help" );
    SetMenuBar( menuBar );

    CreateStatusBar();


	wxFlexGridSizer* flex = new wxFlexGridSizer(2, 2, wxDefaultSize);
	SetSizer(flex);

	// Input frame view
	wxPanel* inputFramePanel = new wxPanel(this, wxID_ANY);
	{
		m_pVideo = new WxVideoView(inputFramePanel, wxID_ANY, wxDefaultPosition, wxSize(320,240));
		wxStaticText* label1 = new wxStaticText(inputFramePanel, wxID_ANY, "Min blob size:");
		m_sldMinBlobSize = new wxSlider(inputFramePanel, SID_MinBlobSize, 1, 0, 1000, wxDefaultPosition, wxDefaultSize, wxSL_VALUE_LABEL);	
		wxStaticText* label2 = new wxStaticText(inputFramePanel, wxID_ANY, "Max blob size:");	
		m_sldMaxBlobSize = new wxSlider(inputFramePanel, SID_MaxBlobSize, 1, 0, 1000, wxDefaultPosition, wxDefaultSize, wxSL_VALUE_LABEL);	
	
		wxStaticBoxSizer* sbox = new wxStaticBoxSizer(wxVERTICAL, inputFramePanel, "Input Frame");
		sbox->Add(m_pVideo, 1, wxEXPAND | wxALL);
		sbox->Add(label1, 0, wxEXPAND | wxALL);
		sbox->Add(m_sldMinBlobSize, 0, wxEXPAND | wxALL);
		sbox->Add(label2, 0, wxEXPAND | wxALL);
		sbox->Add(m_sldMaxBlobSize, 0, wxEXPAND | wxALL);

		sbox->SetSizeHints(inputFramePanel);
		inputFramePanel->SetSizer(sbox);
	}

	// Result frame view
	wxPanel* resultFramePanel = new wxPanel(this, wxID_ANY);
	{
		m_pResultFrame = new WxVideoView(resultFramePanel, wxID_ANY, wxDefaultPosition, wxSize(320, 240));
	
		wxStaticBoxSizer* sbox = new wxStaticBoxSizer(wxVERTICAL, resultFramePanel, "Result Frame");
		wxStaticText* label1 = new wxStaticText(resultFramePanel, wxID_ANY, "Threshold:");
		m_sldThreshold = new wxSlider(resultFramePanel, SID_Threshold, 1, 0, 255, wxDefaultPosition, wxDefaultSize, wxSL_VALUE_LABEL);

		sbox->Add(m_pResultFrame, 0, wxEXPAND | wxALL);
		sbox->Add(label1, 0, wxEXPAND | wxALL);
		sbox->Add(m_sldThreshold, 0, wxEXPAND | wxALL);
		sbox->SetSizeHints(resultFramePanel);
		resultFramePanel->SetSizer(sbox);
	}

	// Combine 2 video frames into a single panel slot - Background and Smooth frames
	wxPanel* backgroundSmoothPanel = new wxPanel(this, wxID_ANY);
	{
		wxBoxSizer* chbox = new wxBoxSizer(wxHORIZONTAL);
		backgroundSmoothPanel->SetSizer(chbox);

		// Background frame view
		wxPanel* backgroundFramePanel = new wxPanel(backgroundSmoothPanel, wxID_ANY);
		{
			wxStaticBoxSizer* namedbox = new wxStaticBoxSizer(wxVERTICAL, backgroundFramePanel, "Background Frame");
			wxFlexGridSizer* sbox = new wxFlexGridSizer(2, 1, 0, 0);
			backgroundFramePanel->SetSizer(namedbox);

			m_pBackgroundFrame = new WxVideoView(backgroundFramePanel, wxID_ANY, wxDefaultPosition, wxSize(160, 120));
			wxButton* button = new wxButton(backgroundFramePanel, BID_Background, "Capture Background");

			sbox->Add(m_pBackgroundFrame, 1, wxEXPAND | wxALL);
			sbox->Add(button, 0, wxEXPAND | wxALL);
			sbox->SetSizeHints(backgroundFramePanel);

			namedbox->Add(sbox, 0, wxEXPAND | wxALL);
			namedbox->SetSizeHints(backgroundFramePanel);
		}

		// Smooth frame view
		wxPanel* smoothFramePanel = new wxPanel(backgroundSmoothPanel, wxID_ANY);
		{
			wxStaticBoxSizer* namedbox = new wxStaticBoxSizer(wxVERTICAL, smoothFramePanel, "Smooth");
			wxFlexGridSizer* sbox = new wxFlexGridSizer(3, 1, 0, 0);
			smoothFramePanel->SetSizer(namedbox);

			m_pSmoothFrame = new WxVideoView(smoothFramePanel, wxID_ANY, wxDefaultPosition, wxSize(160, 120));
			wxStaticText* label1 = new wxStaticText(smoothFramePanel, wxID_ANY, "Smooth:");
			m_sldSmooth = new wxSlider(smoothFramePanel, SID_Smooth, 1, 0, 10, wxDefaultPosition, wxDefaultSize, wxSL_VALUE_LABEL);

			sbox->Add(m_pSmoothFrame, 1, wxEXPAND | wxALL);
			sbox->Add(label1, 0, wxEXPAND | wxALL);
			sbox->Add(m_sldSmooth, 0, wxEXPAND | wxALL);
			sbox->SetSizeHints(smoothFramePanel);

			namedbox->Add(sbox, 0, wxEXPAND | wxALL);
			namedbox->SetSizeHints(smoothFramePanel);
		}

		chbox->Add(backgroundFramePanel, 1, wxEXPAND | wxALL);
		chbox->Add(smoothFramePanel, 1, wxEXPAND | wxALL);
		chbox->SetSizeHints(backgroundSmoothPanel);
	}

	// Combine 2 video frames into a single panel slot - Highpass and Amplify frames
	wxPanel* highpassAmplifyPanel = new wxPanel(this, wxID_ANY);
	{
		wxBoxSizer* chbox = new wxBoxSizer(wxHORIZONTAL);
		highpassAmplifyPanel->SetSizer(chbox);

		// Highpass frame view
		wxPanel* highpassFramePanel = new wxPanel(highpassAmplifyPanel, wxID_ANY);
		{
			wxStaticBoxSizer* namedbox = new wxStaticBoxSizer(wxVERTICAL, highpassFramePanel, "Highpass");
			wxFlexGridSizer* sbox = new wxFlexGridSizer(5, 1, 0, 0);
			highpassFramePanel->SetSizer(namedbox);

			m_pHighpassFrame = new WxVideoView(highpassFramePanel, wxID_ANY, wxDefaultPosition, wxSize(160, 120));
			wxStaticText* label1 = new wxStaticText(highpassFramePanel, wxID_ANY, "Blur:");
			m_sldBlur = new wxSlider(highpassFramePanel, SID_Blur, 1, 0, 100, wxDefaultPosition, wxDefaultSize, wxSL_VALUE_LABEL);
			wxStaticText* label2 = new wxStaticText(highpassFramePanel, wxID_ANY, "Noise:");
			m_sldNoise = new wxSlider(highpassFramePanel, SID_Noise, 1, 0, 10, wxDefaultPosition, wxDefaultSize, wxSL_VALUE_LABEL);

			sbox->Add(m_pHighpassFrame, 1, wxEXPAND | wxALL);
			sbox->Add(label1, 0, wxEXPAND | wxALL);
			sbox->Add(m_sldBlur, 0, wxEXPAND | wxALL);
			sbox->Add(label2, 0, wxEXPAND | wxALL);
			sbox->Add(m_sldNoise, 0, wxEXPAND | wxALL);
			sbox->SetSizeHints(highpassFramePanel);

			namedbox->Add(sbox, 0, wxEXPAND | wxALL);
			namedbox->SetSizeHints(highpassFramePanel);
		}

		// Amplify frame view
		wxPanel* amplifyFramePanel = new wxPanel(highpassAmplifyPanel, wxID_ANY);
		{
			wxStaticBoxSizer* namedbox = new wxStaticBoxSizer(wxVERTICAL, amplifyFramePanel, "Amplify");
			wxFlexGridSizer* sbox = new wxFlexGridSizer(3, 1, 0, 0);			
			amplifyFramePanel->SetSizer(namedbox);

			m_pAmplifyFrame = new WxVideoView(amplifyFramePanel, wxID_ANY, wxDefaultPosition, wxSize(160, 120));
			wxStaticText* label1 = new wxStaticText(amplifyFramePanel, wxID_ANY, "Level:");
			m_sldAmplify = new wxSlider(amplifyFramePanel, SID_AmplifyLevel, 1, 0, 20, wxDefaultPosition, wxDefaultSize, wxSL_VALUE_LABEL);

			sbox->Add(m_pAmplifyFrame, 1, wxEXPAND | wxALL);
			sbox->Add(label1, 0, wxEXPAND | wxALL);
			sbox->Add(m_sldAmplify, 0, wxEXPAND | wxALL);
			sbox->SetSizeHints(amplifyFramePanel);

			namedbox->Add(sbox, 0, wxEXPAND | wxALL);
			namedbox->SetSizeHints(amplifyFramePanel);
		}

		chbox->Add(highpassFramePanel, 1, wxEXPAND | wxALL);
		chbox->Add(amplifyFramePanel, 1, wxEXPAND | wxALL);
		chbox->SetSizeHints(highpassAmplifyPanel);
	}

	// Add panels
	flex->Add(inputFramePanel, 1, wxEXPAND | wxALL);
	flex->Add(resultFramePanel, 1, wxEXPAND | wxALL);
	flex->Add(backgroundSmoothPanel, 1, wxEXPAND | wxALL);
	flex->Add(highpassAmplifyPanel, 1, wxEXPAND | wxALL);

	flex->SetSizeHints(this);

	Centre();

	SetMaxSize(GetSize());
}

WxKitThreadedFrame::~WxKitThreadedFrame()
{
	SAFE_RELEASE(m_pKit);
}

void WxKitThreadedFrame::InitKitThread()
{
    if (CreateThread(wxTHREAD_JOINABLE) != wxTHREAD_NO_ERROR)
    {
        wxLogError("Could not create the worker thread!");
        return;
    }

	SetWorkerAlive(true);

    if (GetThread()->Run() != wxTHREAD_NO_ERROR)
    {
        wxLogError("Could not run the worker thread!");
        return;
    }
}

void WxKitThreadedFrame::OnCalibrationComplete(bool bSuccessful)
{
	// Update status text
	std::string calibrationText = bSuccessful ? "Calibration complete!" : "Calibration cancelled";
	QueueStatusMessage(calibrationText);
}

void WxKitThreadedFrame::OnKitInitialized(wxThreadEvent& event)
{
	UpdateSliders();
}

void WxKitThreadedFrame::OnKitUpdate(wxThreadEvent& event)
{
	if (m_pCalibrationFrame != NULL)
	{
		m_pCalibrationFrame->Update();
	}

	if (m_pVideo != NULL)
	{
		cv::Mat frame = m_pKit->GetTracker()->GetInputFrame();
		if (!frame.empty())
		{
			StopNoCameraInputTimer();

			cv::Mat combinedOutput = cv::Mat::zeros(frame.size(), CV_8UC3);

			DrawDetectionOutput(combinedOutput);
			DrawTrackingOutput(combinedOutput);
			frame = frame + combinedOutput;

			m_pVideo->SetVideoFrame(frame);
		}
		else
		{
			StartNoCameraInputTimer();
		}
	}

	if (m_pBackgroundFrame != NULL)
	{
		cv::Mat frame = m_pKit->GetTracker()->GetBackgroundFrame();
		m_pBackgroundFrame->SetVideoFrame(frame);
	}

	if (m_pResultFrame != NULL)
	{
		cv::Mat frame = m_pKit->GetTracker()->GetResultFrame();
		m_pResultFrame->SetVideoFrame(frame);
	}

	if (m_pSmoothFrame != NULL)
	{
		cv::Mat frame = m_pKit->GetTracker()->GetSmoothedFrame();
		m_pSmoothFrame->SetVideoFrame(frame);
	}

	if (m_pHighpassFrame != NULL)
	{
		cv::Mat frame = m_pKit->GetTracker()->GetHighpassedFrame();
		m_pHighpassFrame->SetVideoFrame(frame);
	}

	if (m_pAmplifyFrame != NULL)
	{
		cv::Mat frame = m_pKit->GetTracker()->GetAmplifiedFrame();
		m_pAmplifyFrame->SetVideoFrame(frame);
	}
}

void WxKitThreadedFrame::SetWorkerAlive(bool alive)
{
	m_workerMutex.Lock();
	m_bWorkerAlive = alive;
	m_workerMutex.Unlock();
}

bool WxKitThreadedFrame::IsWorkerAlive()
{
	bool bAlive;

	m_workerMutex.Lock();
	bAlive = m_bWorkerAlive;
	m_workerMutex.Unlock();

	return bAlive;
}

wxThread::ExitCode WxKitThreadedFrame::Entry()
{
	if (m_pKit == NULL)
	{
		return (wxThread::ExitCode)1;
	}

	m_pKit->Init();
	wxQueueEvent(this, new wxThreadEvent(wxEVT_THREAD, TID_Initialize));

	while (!GetThread()->TestDestroy() && IsWorkerAlive())
	{
		m_pKit->RunFrame();

		// Notify GUI thread
		wxQueueEvent(this, new wxThreadEvent(wxEVT_THREAD, TID_Update));

#if _WIN32
		Sleep(20);
#else
        sleep(20);
#endif
	}

	m_pKit->Shutdown();

	return (wxThread::ExitCode)0;
}

void WxKitThreadedFrame::UpdateSliders()
{
	if (m_pKit != NULL)
	{
		Tracker* tracker = m_pKit->GetTracker();
		if (tracker != NULL)
		{
			// Update UI using new settings
			int minBlobSize = tracker->GetBlobMinAllowedSize();
			int maxBlobSize = tracker->GetBlobMaxAllowedSize();
			int threshold = tracker->GetThreshold();
			int smooth = tracker->GetSmooth();
			int blur = tracker->GetBlur();
			int noise = tracker->GetNoise();
			int amplify = tracker->GetAmplifyLevel();

			m_sldMinBlobSize->SetValue(minBlobSize);
			m_sldMaxBlobSize->SetValue(maxBlobSize);
			m_sldThreshold->SetValue(threshold);
			m_sldSmooth->SetValue(smooth);
			m_sldBlur->SetValue(blur);
			m_sldNoise->SetValue(noise);
			m_sldAmplify->SetValue(amplify);
		}
	}
}

void WxKitThreadedFrame::OnClose(wxCloseEvent&)
{
    // important: before terminating, we _must_ wait for our joinable
    // thread to end, if it's running; in fact it uses variables of this
    // instance and posts events to *this event handler
    if (GetThread() &&      // DoStartALongTask() may have not been called
        GetThread()->IsRunning())
	{
		SetWorkerAlive(false);
        GetThread()->Wait();
	}

    Destroy();
}

void WxKitThreadedFrame::OnExit(wxCommandEvent& event)
{
	Close( true );
}

void WxKitThreadedFrame::OnAbout(wxCommandEvent& event)
{
    wxMessageBox( "This is a wxWidgets' Hello world sample",
                  "About KeepInTouch", wxOK | wxICON_INFORMATION );
}

void WxKitThreadedFrame::OnSettingsLoad(wxCommandEvent& event)
{
	if (m_pKit != NULL && m_pKit->GetSettings() != NULL)
	{
		m_pKit->GetSettings()->Read();
		UpdateSliders();
		QueueStatusMessage("Settings loaded successfully!");
	}
	else
	{
		QueueStatusMessage("Could not load settings");
	}
}

void WxKitThreadedFrame::OnSettingsSave(wxCommandEvent& event)
{
	if (m_pKit != NULL && m_pKit->GetSettings() != NULL)
	{
		m_pKit->GetSettings()->Write();
		QueueStatusMessage("Settings saved successfully!");
	}
	else
	{
		QueueStatusMessage("Could not save settings");
	}
}

void WxKitThreadedFrame::OnCalibrationStart(wxCommandEvent& event)
{
	if (m_pCalibrationFrame == NULL)
	{
		m_pCalibrationFrame = new WxKitCalibrationFrame(this, wxID_ANY, "Calibration", m_pKit);
		m_pCalibrationFrame->RegisterCalibrationEventListener(this);
	}

	m_pCalibrationFrame->StartCalibration();
}

void WxKitThreadedFrame::OnThresholdUpdate(wxCommandEvent& event)
{
	int value = event.GetInt();
	m_pKit->GetTracker()->SetThreshold(value);
}

void WxKitThreadedFrame::OnSmoothUpdate(wxCommandEvent& event)
{
	int value = event.GetInt();
	m_pKit->GetTracker()->SetSmooth(value);
}

void WxKitThreadedFrame::OnBlurUpdate(wxCommandEvent& event)
{
	int value = event.GetInt();
	m_pKit->GetTracker()->SetBlur(value);
}

void WxKitThreadedFrame::OnNoiseUpdate(wxCommandEvent& event)
{
	int value = event.GetInt();
	m_pKit->GetTracker()->SetNoise(value);
}

void WxKitThreadedFrame::OnAmplifyLevelUpdate(wxCommandEvent& event)
{
	int value = event.GetInt();
	m_pKit->GetTracker()->SetAmplifyLevel(value);
}

void WxKitThreadedFrame::OnMinBlobSizeUpdate(wxCommandEvent& event)
{
	int value = event.GetInt();
	m_pKit->GetTracker()->SetBlobMinAllowedSize(value);
}

void WxKitThreadedFrame::OnMaxBlobSizeUpdate(wxCommandEvent& event)
{
	int value = event.GetInt();
	m_pKit->GetTracker()->SetBlobMaxAllowedSize(value);
}

void WxKitThreadedFrame::OnBackgroundButtonPress(wxCommandEvent& event)
{
	m_pKit->GetTracker()->SetCurrentInputFrameAsBackground();
	QueueStatusMessage("Background captured!");
}

void WxKitThreadedFrame::QueueStatusMessage(const wxString& message)
{
	SetStatusText(message);

	if (m_statusUpdateTimer != NULL)
	{
		if (m_statusUpdateTimer->IsRunning())
		{
			m_statusUpdateTimer->Stop();
		}

		const int StatusTextDuration = 3000;
		m_statusUpdateTimer->Start(StatusTextDuration, wxTIMER_ONE_SHOT);
	}
}

void WxKitThreadedFrame::OnStatusUpdateTimer(wxTimerEvent& event)
{
	SetStatusText("");

	if (m_statusUpdateTimer != NULL && m_statusUpdateTimer->IsRunning())
	{
		m_statusUpdateTimer->Stop();
	}
}

void WxKitThreadedFrame::StartNoCameraInputTimer()
{
	if (m_noCameraInputTimer != NULL && !m_noCameraInputTimer->IsRunning())
	{
		const int CameraInputDetectionDuration = 3000;
		m_noCameraInputTimer->Start(CameraInputDetectionDuration, wxTIMER_ONE_SHOT);
	}
}

void WxKitThreadedFrame::StopNoCameraInputTimer()
{
	if (m_noCameraInputTimer != NULL && m_noCameraInputTimer->IsRunning())
	{
		m_noCameraInputTimer->Stop();
	}
}

void WxKitThreadedFrame::OnNoCameraInputTimer(wxTimerEvent& event)
{
	StopNoCameraInputTimer();
	QueueStatusMessage("Could not retrieve camera input!");
}

void WxKitThreadedFrame::DrawDetectionOutput(cv::Mat& drawing)
{
	cv::Scalar contourColor = cv::Scalar(0, 0, 255);
	cv::Scalar rectangleColor = cv::Scalar(0, 255, 0);
	cv::Scalar centerColor = cv::Scalar(255, 0, 0);

	const BlobDetector::Contours blobs =  m_pKit->GetTracker()->GetInputBlobs();
	for (int i = 0; i < blobs.size(); ++i)
	{
		cv::drawContours(drawing, blobs, i, contourColor, 2);

		cv::Mat blob( blobs[i] );

		// Draw bounding rectangle
		cv::Rect rect( cv::boundingRect(blob) );
		cv::rectangle(drawing, rect.tl(), rect.br(), rectangleColor, 2);

		// Draw center
		cv::Point2f center;
		float radius;
		cv::minEnclosingCircle(blob, center, radius);
		cv::circle(drawing, center, 1, centerColor, 2);
	}
}

void WxKitThreadedFrame::DrawTrackingOutput(cv::Mat& drawing)
{
	cv::Scalar textColor = cv::Scalar(255, 255, 255);

	BlobContainer trackedBlobs = m_pKit->GetTracker()->GetOutputBlobs();
	for (int i = 0; i < trackedBlobs.size(); ++i)
	{
		Blob currentBlob = trackedBlobs[i];
		std::ostringstream buffer;
		buffer << currentBlob.id;
		std::string areaText = buffer.str();

		// Denormalize position
		cv::Point center( static_cast<int>(currentBlob.x * drawing.rows), static_cast<int>(currentBlob.y * drawing.cols) );
		cv::putText(drawing, areaText, center, CV_FONT_HERSHEY_SIMPLEX, 0.5f, textColor);
	}
}

} // namespace kit

#endif // USE_WX_WINDOW_GUI
