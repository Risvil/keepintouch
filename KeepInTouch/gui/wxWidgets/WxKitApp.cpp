#include "WxKitApp.h"

#pragma message("Before App...")

#if USE_WX_WINDOW_GUI || 1

#include "WxKitThreadedFrame.h"
#pragma message("[2] Compiling KIT_UI with wxWidgets")
namespace kit
{

wxIMPLEMENT_APP(WxKitApp);

bool WxKitApp::OnInit()
{
	WxKitThreadedFrame* frame = new WxKitThreadedFrame( "KeepInTouch", wxDefaultPosition, wxDefaultSize );
	frame->InitKitThread();
    frame->Show( true );

    return true;
}

int WxKitApp::OnExit()
{
	return 0;
}

} // namespace kit

#endif // USE_WX_WINDOW_GUI
