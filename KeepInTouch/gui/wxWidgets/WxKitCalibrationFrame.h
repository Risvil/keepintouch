#ifndef WXKITCALIBRATIONFRAME_H
#define WXKITCALIBRATIONFRAME_H

#include "WxKitGUI.h"

#if USE_WX_WINDOW_GUI

#include <vector>

namespace kit
{

class IWxCalibrationEventListener
{
public:
	virtual void OnCalibrationComplete(bool bSuccess) = 0;

	virtual ~IWxCalibrationEventListener() {}
};

class WxVideoView;
class WxKitCalibrationFrame : public wxFrame
{
public:
	WxKitCalibrationFrame(wxWindow* parent, wxWindowID id, const wxString& title, KeepInTouch* kit);
	~WxKitCalibrationFrame(void);

	void RegisterCalibrationEventListener(IWxCalibrationEventListener* listener);

	void StartCalibration();
	void StopCalibration(bool bSuccessful);
	void Update();

	void SetVideoFrame(const cv::Mat& frame);

	void OnKeyReleased(wxKeyEvent& event);
	void OnClose(wxCloseEvent&);

	wxDECLARE_EVENT_TABLE();

private:
	void DrawDetectionOutput(cv::Mat& drawing);
	void DrawTrackingOutput(cv::Mat& drawing);

private:
	WxVideoView* m_pVideo;
	KeepInTouch* m_pKit;
	std::vector<IWxCalibrationEventListener*> m_listeners;
};

} // namespace kit

#endif // USE_WX_WINDOW_GUI

#endif // WXKITCALIBRATIONFRAME_H