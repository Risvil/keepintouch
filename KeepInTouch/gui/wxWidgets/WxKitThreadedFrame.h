#ifndef WXKITTHREADEDFRAME_H
#define WXKITTHREADEDFRAME_H

#include "WxKitGUI.h"

#if USE_WX_WINDOW_GUI

#include "WxKitCalibrationFrame.h"

namespace kit
{

class WxVideoView;
class WxKitCalibrationFrame;
class WxKitThreadedFrame : public wxFrame, public wxThreadHelper, IWxCalibrationEventListener
{
public:
    WxKitThreadedFrame(const wxString& title, const wxPoint& pos, const wxSize& size);
	~WxKitThreadedFrame();

	void InitKitThread();

	virtual void OnCalibrationComplete(bool bSuccessful);

private:
	void OnKitInitialized(wxThreadEvent& event);
	void OnKitUpdate(wxThreadEvent& event);

	void SetWorkerAlive(bool alive);
	bool IsWorkerAlive();

	wxThread::ExitCode Entry();
	void OnClose(wxCloseEvent&);

	void UpdateSliders();

    void OnExit(wxCommandEvent& event);
    void OnAbout(wxCommandEvent& event);
	void OnSettingsLoad(wxCommandEvent& event);
	void OnSettingsSave(wxCommandEvent& event);
	void OnCalibrationStart(wxCommandEvent& event);

	void OnThresholdUpdate(wxCommandEvent& event);
	void OnSmoothUpdate(wxCommandEvent& event);
	void OnBlurUpdate(wxCommandEvent& event);
	void OnNoiseUpdate(wxCommandEvent& event);
	void OnAmplifyLevelUpdate(wxCommandEvent& event);
	void OnMinBlobSizeUpdate(wxCommandEvent& event);
	void OnMaxBlobSizeUpdate(wxCommandEvent& event);

	void OnBackgroundButtonPress(wxCommandEvent& event);

	void QueueStatusMessage(const wxString& message);
	void OnStatusUpdateTimer(wxTimerEvent& event);

	void StartNoCameraInputTimer();
	void StopNoCameraInputTimer();
	void OnNoCameraInputTimer(wxTimerEvent& event);

	void DrawTrackingOutput(cv::Mat& drawing);
	void DrawDetectionOutput(cv::Mat& drawing);

	wxDECLARE_EVENT_TABLE();

private:
	KeepInTouch* m_pKit;

	WxKitCalibrationFrame* m_pCalibrationFrame;

	WxVideoView* m_pVideo;
	WxVideoView* m_pBackgroundFrame;
	WxVideoView* m_pResultFrame;
	WxVideoView* m_pSmoothFrame;
	WxVideoView* m_pHighpassFrame;
	WxVideoView* m_pAmplifyFrame;

	// Sliders
	wxSlider* m_sldMinBlobSize;
	wxSlider* m_sldMaxBlobSize;
	wxSlider* m_sldThreshold;
	wxSlider* m_sldSmooth;
	wxSlider* m_sldBlur;
	wxSlider* m_sldNoise;
	wxSlider* m_sldAmplify;

	wxTimer* m_statusUpdateTimer;
	wxTimer* m_noCameraInputTimer;

	wxMutex m_workerMutex;
	bool m_bWorkerAlive;

	// Menu IDs
	enum EMenuID
	{
		MID_Background = 1,
		MID_Calibrate,
	};

	// Thread event ID
	enum EThreadID
	{
		TID_Update = 42,
		TID_Initialize,
	};

	// Slider IDs
	enum ESliderID
	{
		SID_Threshold = 130,
		SID_Smooth = 131,
		SID_Blur = 132,
		SID_Noise = 133,
		SID_AmplifyLevel = 134,
		SID_MinBlobSize = 135,
		SID_MaxBlobSize = 136,
	};

	// Button IDs
	enum EButtonID
	{
		BID_Background = 140,
	};

	// Timer IDs
	enum ETimerID
	{
		TiID_StatusUpdate = 141,
		TiID_NoCameraInput = 142,
	};

};

} // namespace kit

#endif // USE_WX_WINDOW_GUI

#endif // WXKITTHREADEDFRAME_H