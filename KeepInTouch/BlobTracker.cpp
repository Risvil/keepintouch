#include "BlobTracker.h"
#include "kit_common.h"
#include "KitTime.h"

#define _USE_MATH_DEFINES
#include <math.h>

namespace kit
{

/**
 * Class default constructor
 */
BlobTrackerKNN::BlobTrackerKNN()
:	m_lastTrackTime(0),
	m_currentTrackTime(0)
{
}

/**
 * Class destructor
 */
BlobTrackerKNN::~BlobTrackerKNN()
{
}

/**
 * Initializes this component
 */
void BlobTrackerKNN::Init()
{
	m_lastTrackTime = 0;
	m_currentTrackTime = 0;
}

/**
 * Shuts down this component and releases resources
 */
void BlobTrackerKNN::Shutdown()
{
}

void BlobTrackerKNN::TrackBlobs()
{
	if (m_blobs.empty() || m_newBlobs.empty())
	{
		return;
	}

	// Create training samples from currently stored blobs (from last frame)
	cv::Mat trainingSamples = cv::Mat(static_cast<int>(m_blobs.size()), 2, CV_32F);
	for (int i = 0; i < m_blobs.size(); ++i)
	{
		trainingSamples.at<float>(i, 0) = static_cast<float>(m_blobs[i].x);
		trainingSamples.at<float>(i, 1) = static_cast<float>(m_blobs[i].y);
	}

	// Create classification classes, one for blob
	cv::Mat trainingClasses = cv::Mat(static_cast<int>(m_blobs.size()), 1, CV_32F);
	for (int i = 0; i < m_blobs.size(); ++i)
	{
		trainingClasses.at<float>(i) = static_cast<float>(m_blobs[i].id);
	}
    
    cv::Ptr<cv::ml::TrainData> trainData = cv::ml::TrainData::create(trainingSamples, cv::ml::SampleTypes::ROW_SAMPLE, trainingClasses);

	// Train the tracker
    m_knearest = cv::ml::KNearest::create();
    
    m_knearest->setIsClassifier(true);
    m_knearest->setAlgorithmType(cv::ml::KNearest::Types::BRUTE_FORCE);
    m_knearest->setDefaultK(1);
    
    m_knearest->train(trainData);
    
    
	// Create input samples from blobs for this frame
	cv::Mat inputData = cv::Mat(static_cast<int>(m_newBlobs.size()), 2, CV_32F);
	for (int i = 0; i < m_newBlobs.size(); ++i)
	{
		inputData.at<float>(i, 0) = static_cast<float>(m_newBlobs[i].x);
		inputData.at<float>(i, 1) = static_cast<float>(m_newBlobs[i].y);
	}

	// Find class correspondence
	cv::Mat results;
	cv::Mat dists;

    m_knearest->findNearest(inputData, m_knearest->getDefaultK(), results, cv::noArray(), dists);

	AssignIDs(m_newBlobs, results, dists);

	m_lastTrackTime = m_currentTrackTime;
	m_currentTrackTime = KitTime::GetMilliCount();

	LabelDynamicBlobProperties();
}

void BlobTrackerKNN::AssignIDs(BlobContainer& blobs, const cv::Mat& results, const cv::Mat& dists)
{
	// Assign newly found ids, and eliminate duplications by assigning the original id by nearest distance
	for (int i = 0; i < blobs.size(); ++i)
	{
		int newID = static_cast<int>( results.at<float>(i) );

		int duplicatedIdx = IndexOf(blobs, newID);		
		// If the id has appeared already, remove duplications
		if (duplicatedIdx != INDEX_NONE)
		{
			ReasignDuplicatedID(blobs, dists, duplicatedIdx, i, newID);
		}
		// This id has not appeared yet
		else
		{
			blobs[i].id = newID;
		}
	}
}

void BlobTrackerKNN::ReasignDuplicatedID(BlobContainer& blobs, const cv::Mat& dists, int oldIndex, int newIndex, int newID)
{
	float oldDistance = dists.at<float>(oldIndex);
	float newDistance = dists.at<float>(newIndex);

	if (oldDistance <= newDistance)
	{
		blobs[newIndex].id = ABlobTracker::GetNextBlobId();
	}
	else
	{
		// Swap IDs
		blobs[newIndex].id = newID;
		blobs[oldIndex].id = ABlobTracker::GetNextBlobId();
	}
}

void BlobTrackerKNN::LabelDynamicBlobProperties()
{
	// Elapsed time in seconds
	float dt = static_cast<float>(m_currentTrackTime - m_lastTrackTime) / 1000;

	for (int i = 0; i < m_newBlobs.size(); ++i)
	{
		Blob& currentBlob = m_newBlobs[i];

		BlobContainer::const_iterator it = m_blobs.begin();
		BlobContainer::const_iterator endIt = m_blobs.end();
		BlobContainer::const_iterator foundBlob;

		foundBlob = std::find(it, endIt, currentBlob);

		if (foundBlob != endIt)
		{
			float dx = currentBlob.x - foundBlob->x;
			float dy = currentBlob.x - foundBlob->y;

			float vx = dx / dt; 
			float vy = dy / dt;

			cv::Vec<float, 2> currentSpeed(vx, vy);
			cv::Vec<float, 2> oldSpeed(foundBlob->vx, foundBlob->vy);

			float motionAcceleration = static_cast<float>( norm(currentSpeed - oldSpeed) / dt );

			currentBlob.vx = vx;
			currentBlob.vy = vy;
			currentBlob.motionAcceleration = motionAcceleration;
		}
	}
}

} // namespace kit
