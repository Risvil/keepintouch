#include <math.h>

#include "Calibration.h"
#include "KitTime.h"

namespace kit
{

enum PointIndex
{
	PI_TOPLEFT = 0,
	PI_TOPRIGHT,
	PI_BOTTOMLEFT,
	PI_BOTTOMRIGHT,
};

LinearInterpolationCalibrationMethod::LinearInterpolationCalibrationMethod()
	: m_currentPoint(0),
	  m_bCalibrationMode(false),
	  m_updateBlobTimeMillis(3000),
	  m_startingBlobTimeMillis(0)
{
	// Initialize m_calibrationSamples to be the 4 image corners -> Need image resolution? Use relative positioning if possible instead;
	FloatPoint topLeft(0, 0);
	FloatPoint topRight(1, 0);
	FloatPoint bottomLeft(0, 1);
	FloatPoint bottomRight(1, 1);

	m_calibrationSamples.push_back(topLeft);
	m_calibrationSamples.push_back(topRight);
	m_calibrationSamples.push_back(bottomLeft);
	m_calibrationSamples.push_back(bottomRight);
}

void LinearInterpolationCalibrationMethod::StartCalibration()
{
	m_points.clear();
	m_currentPoint = 0;
	m_bCalibrationMode = true;
	m_startingBlobTimeMillis = 0;
	m_currentBlob.Invalidate();
}

void LinearInterpolationCalibrationMethod::CancelCalibration()
{
	m_bCalibrationMode = false;
}

void LinearInterpolationCalibrationMethod::UpdateCalibration(const BlobContainer& blobs)
{
	// Invalidate when inappropiate input or when it's calibrated already
	if (blobs.size() == 0 || blobs.size() > 1 || IsCalibrated())
	{
		m_currentBlob.Invalidate();
		return;
	}

	Blob blob = blobs.front();

	// Don't allow same blob to be used more than once as calibration spot
	if (m_currentPoint > 0)
	{
		const Blob& lastPoint = m_points[m_currentPoint-1];
		if (lastPoint == blob)
		{
			return;
		}
	}

	// Check if blob is in same position as in last frame
    const float epsilon = 0.02f;
	bool bSamePosition = fabs(m_currentBlob.x - blob.x) <= epsilon && fabs(m_currentBlob.y - blob.y) <= epsilon;

	// If same blob being detected during the specified time, consider it 
	// as the current calibration point
	if (m_currentBlob.IsValid() && m_currentBlob == blob && bSamePosition)
	{
		int elapsedTime = KitTime::GetMilliSpan(m_startingBlobTimeMillis);
		if (elapsedTime >= m_updateBlobTimeMillis)
		{
			// Calibrated. Add to point list
			++m_currentPoint;
			m_points.push_back(blob);

			// Restart current blob value
			m_currentBlob.Invalidate();

			if (IsCalibrated())
			{
				// Get maximum top-left corner captured
				m_calibratedTopLeft.x = fmaxf(m_points[PI_TOPLEFT].x, m_points[PI_BOTTOMLEFT].x);
				m_calibratedTopLeft.y = fmaxf(m_points[PI_TOPLEFT].y, m_points[PI_TOPRIGHT].y);

				// Get minimun bottom-right corner captured
				m_calibratedBottomRight.x = fminf(m_points[PI_TOPRIGHT].x, m_points[PI_BOTTOMRIGHT].x);
				m_calibratedBottomRight.y = fminf(m_points[PI_BOTTOMLEFT].y, m_points[PI_BOTTOMRIGHT].y);
			}
		}
	}
	else
	{
		// Set this blob as a candidate to calibration point
		m_currentBlob = blob;
		m_startingBlobTimeMillis = KitTime::GetMilliCount();
	}
}

void LinearInterpolationCalibrationMethod::ApplyCalibration(Blob& blob) const
{
	if (IsCalibrated())
	{
		blob.x = LinearInterpolation<float>(0.f, 1.f, m_calibratedTopLeft.x, m_calibratedBottomRight.x, blob.x);
		blob.y = LinearInterpolation<float>(0.f, 1.f, m_calibratedTopLeft.y, m_calibratedBottomRight.y, blob.y);
	}
}

bool LinearInterpolationCalibrationMethod::IsCalibrated() const
{
	return m_points.size() >= m_calibrationSamples.size();
}

const FloatPoint& LinearInterpolationCalibrationMethod::GetCurrentCalibrationPoint() const
{
	return m_calibrationSamples[m_currentPoint];
}

float LinearInterpolationCalibrationMethod::GetCurrentPointCalibrationPercentage() const
{
	if (!m_currentBlob.IsValid())
	{
		return 0.f;
	}

	int elapsedTime = KitTime::GetMilliSpan(m_startingBlobTimeMillis);
	float percentage = elapsedTime / static_cast<float>(m_updateBlobTimeMillis);

	return percentage;
}

template<class T>
T LinearInterpolationCalibrationMethod::LinearInterpolation(const T& a0, const T& a1, const T& b0, const T& b1, const T& p) const
{
	// Do not interpolate if p is outside range
	if (p > a1 || p < a0)
	{
		return p;
	}

	// Get % in a0-a1 range
	T u = (p - a0) / (a1 - a0);
	// Get corresponding number of that % in range b0-b1
	T n = (b1 - b0)*u + b0;

	return n;
}

} // namespace kit
