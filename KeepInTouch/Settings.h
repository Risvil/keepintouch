#ifndef SETTINGS_H
#define SETTINGS_H

#include "kit_interfaces.h"

namespace kit
{

class Tracker;
class PlainFileSettings : public ISettings
{
public:
	PlainFileSettings(Tracker* tracker);

	virtual ~PlainFileSettings();

	virtual void Read();
	virtual void Write();

private:
	Tracker* m_pTracker;

	static const std::string m_settingsFilename;
};

} // namespace kit

#endif // SETTINGS_H