#ifndef KITTIME_H
#define KITTIME_H

namespace kit
{

class KitTime
{
public:	
	static int GetMilliCount();
	static int GetMilliSpan(int nTimeStart);

private:
	KitTime();
	KitTime(const KitTime& rhs);
	KitTime& operator=(const KitTime& rhs);
};

} // namespace kit

#endif // KITTIME_H
