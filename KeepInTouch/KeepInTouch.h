#ifndef KEEPINTOUCH_H
#define KEEPINTOUCH_H

#include "Build.h"
#include "kit_interfaces.h"

namespace kit
{

class Tracker;
class ICalibrationMethod;

class KeepInTouch : public IObject
{
public:
	KeepInTouch();
	virtual ~KeepInTouch();

	virtual void Init();
	virtual void Shutdown();

	void RunFrame() const;

	void StartCalibration();
	void StopCalibration();
	bool IsCalibrationMode() const;

	inline Tracker* GetTracker() const
	{
		return m_pTracker;
	}
	inline ISettings* GetSettings() const
	{
		return m_pSettings;
	}
	ICalibrationMethod* GetCalibrator() const;

private:
	KeepInTouch(const KeepInTouch&);
	KeepInTouch& operator=(const KeepInTouch&);

private:
	Tracker* m_pTracker;
	ISettings* m_pSettings;
};

} // namespace kit

#endif // KEEPINTOUCH_H