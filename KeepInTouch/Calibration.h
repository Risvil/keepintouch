#ifndef CALIBRATION_H
#define CALIBRATION_H

#include "kit_common.h"

namespace kit
{

class ICalibrationMethod
{
public:
	virtual void StartCalibration() = 0;
	virtual void CancelCalibration() = 0;
	virtual void UpdateCalibration(const BlobContainer& blobs) = 0;
	virtual void ApplyCalibration(Blob& blob) const = 0;
	virtual bool IsCalibrated() const = 0;
	virtual bool IsCalibrationMode() const = 0;
	virtual const FloatPoint& GetCurrentCalibrationPoint() const = 0;
	virtual float GetCurrentPointCalibrationPercentage() const = 0;

	virtual ~ICalibrationMethod() {}
};

class LinearInterpolationCalibrationMethod : public ICalibrationMethod
{
public:
	LinearInterpolationCalibrationMethod();

	virtual void StartCalibration();
	virtual void CancelCalibration();
	virtual void UpdateCalibration(const BlobContainer& blobs);
	virtual void ApplyCalibration(Blob& blob) const;

	inline virtual bool IsCalibrationMode() const
	{
		return m_bCalibrationMode;
	}

	virtual bool IsCalibrated() const;
	virtual const FloatPoint& GetCurrentCalibrationPoint() const;
	virtual float GetCurrentPointCalibrationPercentage() const;

private:
	template<class T>
	T LinearInterpolation(const T& a0, const T& a1, const T& b0, const T& b1, const T& p) const;

private:
	std::vector<Blob> m_points;
	std::vector<FloatPoint> m_calibrationSamples;
	int m_currentPoint;
	bool m_bCalibrationMode;

	const int m_updateBlobTimeMillis;
	int m_startingBlobTimeMillis;
	Blob m_currentBlob;

	FloatPoint m_calibratedTopLeft;
	FloatPoint m_calibratedBottomRight;
};

} // namespace kit

#endif // CALIBRATION_H