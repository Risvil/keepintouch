#ifndef BLOBDETECTOR_H
#define BLOBDETECTOR_H

#include <vector>
#include <opencv2/opencv.hpp>
#include "kit_common.h"
#include "kit_interfaces.h"

namespace kit
{

/**
 * Detects existing blobs (countours) inside an input image
 *
 * @author: Ruben Avalos Elvira
 * @version: 1.0
 */
class ICalibrationMethod;
class BlobDetector : public IObject
{
public:
	typedef std::vector<cv::Point> Contour;
	typedef std::vector<Contour> Contours;

	/**
	 * Basic constructor
	 */
	BlobDetector();

	/**
	 * Class destructor
	 */
	virtual ~BlobDetector();

	/**
	 * Initializes this component
	 */
	virtual void Init();

	/**
	 * Shuts down this component and releases resources
	 */
	virtual void Shutdown();

	/**
	 * Gets the calibration method being used by the detection and labeling process
	 *
	 * @return Calibration method being used
	 */
	inline ICalibrationMethod* GetCalibrator() const
	{
		return m_pCalibrator;
	}

	/**
	 * Sets the frame to be used by the detection process
	 *
	 * @param inputFrame Frame to be used
	 */
	inline void SetInputFrame(const cv::Mat& inputFrame)
	{
		m_inputFrame = inputFrame;
	}

	/**
	 * Gets the current frame being used by the detection process
	 *
	 * @return Current frame being used
	 */
	inline const cv::Mat& GetInputFrame() const
	{
		return m_inputFrame;
	}

	/**
	 * Detects blobs in an image
	 */
	void DetectBlobs();

	/**
	 * Gets the blobs detected
	 *
	 * @return Contours found by detection process
	 */
	const Contours& GetBlobs() const
	{
		return m_blobs;
	}

	/**
	 * Gets the blobs detected, including all the information about each blob
	 *
	 * @return Blobs found and their details
	 */
	BlobContainer& GetLabeledBlobs()
	{
		return m_labeledBlobs;
	}

	/**
	 * Gets the minimum size for a blob to be considered of interest
	 *
	 * @return Minimum blob size
	 */
	int GetBlobMinAllowedSize() const
	{
		return m_blobMinAllowedSize;
	}

	/**
	 * Gets the minimum size for a blob to be considered of interest
	 *
	 * @param size New minimum blob size
	 */
	void SetBlobMinAllowedSize(int size)
	{
		m_blobMinAllowedSize = (size > MIN_BLOB_SIZE) ? size : MIN_BLOB_SIZE;
	}

	/**
	 * Gets the maximum size for a blob to be considered of interest
	 *
	 * @return Maximum blob size
	 */
	int GetBlobMaxAllowedSize() const
	{
		return m_blobMaxAllowedSize;
	}

	/**
	 * Sets the maximum size for a blob to be considered of interest
	 *
	 * @param size New maximum blob size
	 */
	void SetBlobMaxAllowedSize(int size)
	{
		m_blobMaxAllowedSize = (size < MAX_BLOB_SIZE) ? size : MAX_BLOB_SIZE;
	}

public:
	/** Minimum blob size allowed by the system */
	static const int MIN_BLOB_SIZE;
	/** Maximum blob size allowed by the system */
	static const int MAX_BLOB_SIZE;

private:
	/**
	 * Sets up all the additional information needed for a full labeled blob, from the unlabeled ones.
	 */
	void LabelBlobs();

	/**
	 * Applies calibration method to a blob position
	 */
	void ApplyCalibration(Blob& blob) const;

private:
	/** Frame being used to detect blobs */
	cv::Mat m_inputFrame;

	/** Blobs found inside input frame */
	Contours m_blobs;

	/** Blobs found inside input frame along info about them */
	BlobContainer m_labeledBlobs;

	/** Minimum size for a blob to be considered */
	int m_blobMinAllowedSize;

	/** Maximum size for a blob to be considered */
	int m_blobMaxAllowedSize;

	/** Calibration method to be used after blob detection */
	ICalibrationMethod* m_pCalibrator;
};

} // namespace kit

#endif // BLOBDETECTOR_H
