#include <fstream>
#include "Settings.h"
#include "Tracker.h"

namespace kit
{

const std::string PlainFileSettings::m_settingsFilename = "settings.cfg";

PlainFileSettings::PlainFileSettings(Tracker* tracker)
	: m_pTracker(tracker)
{
}

PlainFileSettings::~PlainFileSettings()
{
}

void PlainFileSettings::Read()
{
	std::fstream file;

	file.open(m_settingsFilename, std::ios::in);
	std::string ignore;
	int value;

	// Title
	file >> ignore;

	// Threshold
	file >> ignore;
	file >> value;
	m_pTracker->SetThreshold(value);

	// Smooth
	file >> ignore;
	file >> value;
	m_pTracker->SetSmooth(value);

	// Blur
	file >> ignore;
	file >> value;
	m_pTracker->SetBlur(value);

	// Noise
	file >> ignore;
	file >> value;
	m_pTracker->SetNoise(value);

	// Amplify
	file >> ignore;
	file >> value;
	m_pTracker->SetAmplifyLevel(value);

	// Blob min size
	file >> ignore;
	file >> value;
	m_pTracker->SetBlobMinAllowedSize(value);

	// Blob max size
	file >> ignore;
	file >> value;
	m_pTracker->SetBlobMaxAllowedSize(value);

	file.close();
}

void PlainFileSettings::Write()
{
	std::fstream file;

	file.open(m_settingsFilename, std::ios::out);

	file << "KEEPINTOUCH-PlainFileSettings" << std::endl;

	file << "Threshold: " << m_pTracker->GetThreshold() << std::endl;
	file << "Smooth: " << m_pTracker->GetSmooth() << std::endl;
	file << "Blur: " << m_pTracker->GetBlur() << std::endl;
	file << "Noise: " << m_pTracker->GetNoise() << std::endl;
	file << "Amplify: " << m_pTracker->GetAmplifyLevel() << std::endl;
	file << "BlobMinAllowedSize: " << m_pTracker->GetBlobMinAllowedSize() << std::endl;
	file << "BlobMaxAllowedSize: " << m_pTracker->GetBlobMaxAllowedSize() << std::endl;

	file.close();
}

} // namespace kit