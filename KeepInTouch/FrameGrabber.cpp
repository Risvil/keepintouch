#include "FrameGrabber.h"

namespace kit
{

/**
 * Class destructor
 */
FrameGrabber::~FrameGrabber()
{
}

/**
 * Initializes this component
 */
void FrameGrabber::Init()
{
	// Defaults to get input from camera 0
	Init(0);

	// And set frame size to 320x240
	SetFrameSize(320, 240);
}

/**
 * Initializes the FrameGrabber
 */
void FrameGrabber::Init(int cameraNumber)
{
	// Get access to the camera	
	m_capture.open(cameraNumber);
	if (!m_capture.isOpened())
	{
		std::cerr << "ERROR: Could not access the camera!" << std::endl;
		//exit(1);
	}
}

/**
 * Initializes the FrameGrabber to be used with a video
 */
void FrameGrabber::Init(const std::string& videoPath)
{
	m_capture.open(videoPath);
	if (!m_capture.isOpened())
	{
		std::cerr << "ERROR: Could not access the video!" << std::endl;
		//exit(1);
	}
}

/**
 * Shuts down the frame grabber, releasing the used resources properly
 */
void FrameGrabber::Shutdown()
{
	m_capture.release();
}

/**
 * Sets the desired frame size for the retrieved frames
 */
void FrameGrabber::SetFrameSize(int width, int height)
{
	// Try to set the camera resolution
	m_capture.set(CV_CAP_PROP_FRAME_WIDTH, width);
	m_capture.set(CV_CAP_PROP_FRAME_HEIGHT, height);
}

/**
 * Returns the next frame in the input stream
 */
const cv::Mat& FrameGrabber::GetNextFrame()
{	
	if (m_capture.isOpened())
	{
		m_capture >> m_captureFrame;
		if (m_captureFrame.empty())
		{
			std::cerr << "ERROR: Couldn't grab a camera frame." << std::endl;
			m_capture.release();
			//exit(1);
		}
	}
	else
	{
		Init();
	}

	return m_captureFrame;
}

} // namespace kit