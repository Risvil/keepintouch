#ifndef BLOBTRACKER_H
#define BLOBTRACKER_H

#include <vector>
#include <opencv2/ml/ml.hpp>
#include "kit_common.h"
#include "kit_interfaces.h"

namespace kit
{

class ABlobTracker : public IObject
{
public:
	virtual ~ABlobTracker() {}

	virtual void TrackBlobs() = 0;

	void SetInputBlobs(BlobContainer& blobs)
	{
		// Save last frame's input to be used as reference
		m_blobs = m_newBlobs;

		// Set new blobs
		m_newBlobs = blobs;
	}

	const BlobContainer& GetOutputBlobs() const
	{
		return m_newBlobs;
	}

	inline static int GetNextBlobId()
	{
		static int NextId = 255;
		return NextId++;
	}

protected:
	BlobContainer m_blobs;
	BlobContainer m_newBlobs;
};


class BlobTrackerKNN : public ABlobTracker
{
public:

	/**
	 * Class default constructor
	 */
	BlobTrackerKNN();

	virtual ~BlobTrackerKNN();

	virtual void Init();
	virtual void Shutdown();

	virtual void TrackBlobs();

private:
	void AssignIDs(BlobContainer& blobs, const cv::Mat& results, const cv::Mat& dists);
	void ReasignDuplicatedID(BlobContainer& blobs, const cv::Mat& dists, int oldIndex, int newIndex, int newID);
	void LabelDynamicBlobProperties();

private:
    cv::Ptr<cv::ml::KNearest> m_knearest;
	int m_lastTrackTime;
	int m_currentTrackTime;
};

} // namespace kit

#endif // BLOBTRACKER_H
