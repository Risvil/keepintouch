#include "Build.h"
#include "KeepInTouch.h"
#include "kit_utils.h"
#include "Tracker.h"
#include "Settings.h"
#include "Calibration.h"


namespace kit
{

KeepInTouch::KeepInTouch()
{
	m_pTracker = new Tracker();
	m_pSettings = new PlainFileSettings(m_pTracker);
}

KeepInTouch::~KeepInTouch()
{
	SAFE_RELEASE(m_pTracker);
	SAFE_RELEASE(m_pSettings);
}

void KeepInTouch::Init()
{
	if (GetTracker() != NULL)
	{
		GetTracker()->Init();
	}

	if (GetSettings() != NULL)
	{
		GetSettings()->Read();
	}
}

void KeepInTouch::Shutdown()
{
	if (GetTracker() != NULL)
	{
		GetTracker()->Shutdown();
	}
}

void KeepInTouch::RunFrame() const
{
	if (GetTracker() != NULL)
	{
		GetTracker()->PerformDetection();
	}
}

void KeepInTouch::StartCalibration()
{
	if (GetCalibrator() != NULL)
	{
		GetCalibrator()->StartCalibration();
	}
}

void KeepInTouch::StopCalibration()
{
	if (GetCalibrator() != NULL)
	{
		GetCalibrator()->CancelCalibration();
	}
}

bool KeepInTouch::IsCalibrationMode() const
{
	return GetCalibrator() != NULL && GetCalibrator()->IsCalibrationMode();
}

ICalibrationMethod* KeepInTouch::GetCalibrator() const
{
	return (GetTracker() != NULL) ? GetTracker()->GetCalibrator() : NULL;
}

} // namespace kit
