#ifndef KIT_COMMON_H
#define KIT_COMMON_H

#include <vector>

namespace kit
{

template <class T>
struct Point
{
	Point()
		: x(0), y(0) 
	{}

	Point(const T& _x, const T& _y)
		: x(_x), y(_y)
	{}

	T x;
	T y;
};

typedef Point<float> FloatPoint;

enum EIndexValues { INDEX_NONE = -1 };

struct Blob
{
	enum BlobID { ID_INVALID = -1 };

	Blob()
	{
		memset(this, 0, sizeof(Blob));
	}

	bool operator==(const Blob& rhs) const
	{
		return id == rhs.id;
	}

	void Invalidate()
	{
		id = ID_INVALID;
	}

	bool IsValid() const
	{
		return id != ID_INVALID;
	}

	int id;
	float x, y;
	float angle;
	float width, height;
	float area;
	float vx, vy;
	float rotationVelocity;
	float motionAcceleration;
	float rotationAcceleration;
};

typedef std::vector<Blob> BlobContainer;

static int IndexOf(const BlobContainer& blobs, int id)
{
	for (int i = 0; i < blobs.size(); ++i)
	{
		if (blobs[i].id == id)
		{
			return i;
		}
	}

	return INDEX_NONE;
}

} // namespace kit

#endif // KIT_COMMON_H