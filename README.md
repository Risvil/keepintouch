Welcome to KeepInTouch!
=====================

### Disclaimer: This is a work in progress. ###

**Keep-In Touch (KIT)** is a cross-platform solution for computer vision and machine sensing. It takes an video input stream and outputs tracking data (e.g. coordinates and blob size) and events (e.g. finger down, moved and released) that are used in building multi-touch applications. **KeepInTouch** can interface with various web cameras and video devices as well as connect to TUIO/OSC enabled applications and supports many multi-touch lighting techniques including: FTIR, DI, DSI, and LLP.

----------